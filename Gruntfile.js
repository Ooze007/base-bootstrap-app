module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-lesslint');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.initConfig({
		less: {
			developmentBootstrap: {
				options: {
					paths: ['app/lib/bootstrap/less'],
					strictMath: true
				},
				files: {
					'app/pages/styles/bootstrap.css': 'app/lib/bootstrap/less/bootstrap.less'
				}
			},
			development: {
				options: {
					paths: ['app/less']
				},
				files: {
					'app/pages/styles/style.css': 'app/less/style.less'
				}
			},
			productionBootstrap: {
				options: {
					paths: ['app/lib/bootstrap/less'],
					strictMath: true,
					compress: true
				},
				files: {
					'dist/styles/bootstrap.css': 'app/lib/bootstrap/less/bootstrap.less'
				}
			},
			production: {
				options: {
					paths: ['app/less'],
					compress: true
				},
				files: {
					'dist/styles/style.css': 'app/less/style.less'
				}
			}
		},

		concat: {
			options: {
				//banner: '<%= banner %>\n<%= jqueryCheck %>\n<%= jqueryVersionCheck %>',
				//stripBanners: false
			},
			bootstrapDev: {
				src: [
					'app/lib/bootstrap/js/transition.js',
					'app/lib/bootstrap/js/alert.js',
					'app/lib/bootstrap/js/button.js',
					'app/lib/bootstrap/js/carousel.js',
					'app/lib/bootstrap/js/collapse.js',
					'app/lib/bootstrap/js/dropdown.js',
					'app/lib/bootstrap/js/modal.js',
					'app/lib/bootstrap/js/tooltip.js',
					'app/lib/bootstrap/js/popover.js',
					'app/lib/bootstrap/js/scrollspy.js',
					'app/lib/bootstrap/js/tab.js',
					'app/lib/bootstrap/js/affix.js'
				],
				dest: 'app/pages/js/bootstrap.js'
			},
			bootstrapProd: {
				src: [
					'app/lib/bootstrap/js/transition.js',
					'app/lib/bootstrap/js/alert.js',
					'app/lib/bootstrap/js/button.js',
					'app/lib/bootstrap/js/carousel.js',
					'app/lib/bootstrap/js/collapse.js',
					'app/lib/bootstrap/js/dropdown.js',
					'app/lib/bootstrap/js/modal.js',
					'app/lib/bootstrap/js/tooltip.js',
					'app/lib/bootstrap/js/popover.js',
					'app/lib/bootstrap/js/scrollspy.js',
					'app/lib/bootstrap/js/tab.js',
					'app/lib/bootstrap/js/affix.js'
				],
				dest: 'dist/js/bootstrap.js'
			}
		},

		uglify: {
			options: {
				compress: {
					warnings: false
				},
				mangle: true,
				preserveComments: 'some'
			},
			bootstrap: {
				src: '<%= concat.bootstrapProd.dest %>',
				dest: 'dist/js/bootstrap.min.js'
			},
		},

		lesslint: {
			src: ['app/less/*.less'],
			options: {
				imports: [
					'**/*.less'
				]
			}
		},

		jshint: {
			options: {
				jshintrc: 'app/lib/bootstrap/js/.jshintrc'
			},
			bootstrap: {
				src: 'app/lib/bootstrap/js/*.js'
			},
			app: {
				src: 'app/js/*.js'
			}
		},

		jade: {
			dev: {
				options: {
					pretty: '\t',
					data: {
						debug: true
					}
				},
				files:[{
					expand: true,
					cwd: 'app/jade/pages/',
					src: ['*.jade'],
					dest: 'app/pages/',
					ext: '.html',
				}]
			},
			prod: {
				options: {
					pretty: '\t',
					data: {
						debug: false
					}
				},
				files:[{
					expand: true,
					cwd: 'app/jade/pages/',
					src: ['*.jade'],
					dest: 'dist/',
					ext: '.html',
				}]
			}
		},

		copy: {
			dev: {
				expand: true,
				cwd: 'app/lib/bootstrap/fonts/',
				src: ['**'],
				dest: 'app/pages/fonts/'
			},
			prod: {
				expand: true,
				cwd: 'app/lib/bootstrap/fonts/',
				src: ['**'],
				dest: 'dist/fonts/'
			},
			devimg: {
				expand: true,
				cwd: 'app/images/',
				src: ['**'],
				dest: 'app/pages/images/'
			},
			prodimg: {
				expand: true,
				cwd: 'app/images/',
				src: ['**'],
				dest: 'dist/images/'
			}
		},

		watch: {
			jadedev: {
				files: ['app/jade/**/*.jade'],
				tasks: ['jade:dev'],
				options: {
					livereload: true
				},
			},
			jadeprod: {
				files: ['app/jade/**/*.jade'],
				tasks: ['jade:prod'],
				options: {
					livereload: true
				},
			},
			bootstrapdev: {
				files: ['app/lib/bootstrap/less/**/*.less'],
				tasks: ['less:developmentBootstrap'],
				options: {
					livereload: true
				},
			},
			appdev: {
				files: ['app/less/**/*.less'],
				tasks: ['less:development'],
				options: {
					livereload: true
				},
			},
			appimg: {
				files: ['app/images/**/*.*'],
				tasks: ['copy:devimg'],
				options: {
					livereload: true
				},
			}
		},

		connect: {
			options: {
				host: '*',
				base: 'app/pages'
			},
			webserver: {
				options: {
					port: 9009,
					keepalive: true,
					base: 'dist'
				}
			},
			devserver: {
				options: {
					port: 9006,
					//keepalive: true,
					middleware: function(connect, options, middlewares) {
						middlewares.unshift(function(req, res, next) {
							if (req.url !== '/pages')
								return next();

							res.end('pages');
						});

						return middlewares;
					}
				}
			}
		}
	});

	grunt.registerTask('test-js', ['jshint:bootstrap', 'jshint:app']);

	//defaults
	grunt.registerTask('default', ['build']);

	grunt.registerTask('lint', ['lesslint']);

	grunt.registerTask('dev-js', ['concat:bootstrapDev']);
	grunt.registerTask('prod-js', ['concat:bootstrapProd', 'uglify:bootstrap']);
	grunt.registerTask('dev-jade', ['jade:dev']);
	grunt.registerTask('prod-jade', ['jade:prod']);

	grunt.registerTask('watch-dev', ['watch:jadedev', 'watch:bootstrapdev', 'watch:appdev']);

	//production build
	grunt.registerTask('build', ['less:productionBootstrap', 'lesslint', 'less:production', 'prod-js', 'prod-jade', 'copy:prod', 'copy:prodimg']);

	//development build
	grunt.registerTask('devbuild', ['less:developmentBootstrap', 'less:development', 'dev-js', 'dev-jade', 'copy:dev', 'copy:devimg']);

	//development
	grunt.registerTask('dev', ['devbuild', 'connect:devserver', 'watch']);

	//server daemon
	grunt.registerTask('run', ['build', 'connect:webserver']);
}